#include "MAX14830.h"

/*************************************************/
/************** Function Defnitions **************/
/*************************************************/

/*** Initialization functions ***/

//MAX14830 chip initialization funciton - initializes SPI communication, resets all UARTs, enables crystal oscillator
//Invoked implicitly through constructor when object is created
void MAX14830 :: MAX14830_init() {
    SPI_setup();
    UART_resetAll();

    UART_selectUART(UART0);
    SPI_updateRegister(CLKSRC_REG, CLKSRC_CRYST_BIT, SET_BIT);
}

//Resets all UART modules
void MAX14830 :: UART_resetAll() {
    UART_selectUART(UART0);
    SPI_updateRegister(MODE2_REG, MODE2_RST_BIT, SET_BIT);
    SPI_updateRegister(MODE2_REG, MODE2_RST_BIT, CLR_BIT);
    UART_selectUART(UART1);
    SPI_updateRegister(MODE2_REG, MODE2_RST_BIT, SET_BIT);
    SPI_updateRegister(MODE2_REG, MODE2_RST_BIT, CLR_BIT);
    UART_selectUART(UART2);
    SPI_updateRegister(MODE2_REG, MODE2_RST_BIT, SET_BIT);
    SPI_updateRegister(MODE2_REG, MODE2_RST_BIT, CLR_BIT);
    UART_selectUART(UART3);
    SPI_updateRegister(MODE2_REG, MODE2_RST_BIT, SET_BIT);
    SPI_updateRegister(MODE2_REG, MODE2_RST_BIT, CLR_BIT);
}

/*** SPI functions ***/

//Initialize SPI communication with MAX14830
void MAX14830 :: SPI_setup() {
    SPI.begin(SPI_MODE_MASTER);
    SPI.setBitOrder(MSBFIRST);
    
    SPI.setClockSpeed(26, MHZ);
    
    SPI.setDataMode(SPI_MODE0);
    digitalWrite(spi_ss_pin, HIGH);
}

//SPI read register given the register address
uint8_t MAX14830 :: SPI_readRegister(uint8_t addr) {
    addr |= getUARTBitmask(active_UART_module);
    addr &= 0x7F;
    uint8_t rx_buffer[2] = {0x00, 0x00};
    uint8_t tx_buffer[2] = {addr, 0x00};
    digitalWrite(spi_ss_pin, LOW);
    SPI.transfer(tx_buffer, rx_buffer, 2, NULL);
    digitalWrite(spi_ss_pin, HIGH);
    return rx_buffer[1];
}

//SPI write to register given the register address and data to be written
void MAX14830 :: SPI_writeRegister(uint8_t addr, uint8_t data) {
    addr |= getUARTBitmask(active_UART_module);
    addr |= 0x80;
    uint8_t tx_buffer[2] = {addr, data};
    tx_buffer[0] = addr;
    tx_buffer[1] = data;
    digitalWrite(spi_ss_pin, LOW);
    SPI.transfer(tx_buffer, NULL, 2, NULL);
    digitalWrite(spi_ss_pin, HIGH);
}

//SPI update register value given the register address, bits to be changed, type of updation (set bit or clear bit)
void MAX14830 :: SPI_updateRegister(uint8_t addr, uint8_t data, RegisterUpdateType updateType) {
    uint8_t regData = SPI_readRegister(addr);
    if(updateType == SET_BIT) {
        regData |= data;
    }
    else if(updateType == CLR_BIT) {
        regData &= ~data;
    }
    SPI_writeRegister(addr, regData);
}

//SPI check if bit in register is set/cleared(1/0) given the register address bit to be checked
// returns true if bit is set(1) and false when bit is cleared(0)
bool MAX14830 :: SPI_checkBit(uint8_t addr, uint8_t bitmask) {
    if(SPI_readRegister(addr) & bitmask) {
        return true;
    }
    else {
        return false;
    }
}

/*** Clock configuration ***/

//Disbales UART clocking for active UART (receiver/transmitter will not be clocking data in/out)
void MAX14830 :: UART_disableClock() {
    SPI_updateRegister(BRGCFG_REG, BRGCFG_CLKDISABL_BIT, SET_BIT);
}

//Enables UART clocking for active UART
void MAX14830 :: UART_enableClock() {
    SPI_updateRegister(BRGCFG_REG, BRGCFG_CLKDISABL_BIT, CLR_BIT);
}

//Checks if UART clock has been configured and ready for operation for the active UART
//returns true if clock is ready for operation, otherwise returns false
bool MAX14830 :: UART_checkClockReady() {
    return SPI_checkBit(STSINT_REG, STS_CLKREADY_BIT);
}

//Enable PLL to multiply clock
void MAX14830 :: UART_enablePLL() {
    UART_selectUART(UART0);
    SPI_updateRegister(CLKSRC_REG, CLKSRC_PLLBYP_BIT, CLR_BIT);
    SPI_updateRegister(CLKSRC_REG, CLKSRC_PLL_BIT, SET_BIT);
}

//PLL pre-divider configuration (supported values - 1 to 63)
void MAX14830 :: UART_setPLLPredivider(uint8_t divider) {
    UART_selectUART(UART0);
    if(divider < 64) {
        SPI_updateRegister(PLLCONFIG_REG, divider, SET_BIT);
    }
}

//PLL multiplier configurations (Currently supported values - 6,48)
void MAX14830 :: UART_setPLLMultiplier(uint8_t multiplier) {
    UART_selectUART(UART0);
    if(multiplier == 6) {
        SPI_updateRegister(PLLCONFIG_REG, (PLLCFG_PLLFACTOR0 | PLLCFG_PLLFACTOR1), CLR_BIT);
    }
    else if(multiplier == 48) {
        SPI_updateRegister(PLLCONFIG_REG, PLLCFG_PLLFACTOR0, SET_BIT);
        SPI_updateRegister(PLLCONFIG_REG, PLLCFG_PLLFACTOR1, CLR_BIT);
    }
}

//Disable PLL
void MAX14830 :: UART_disablePLL() {
    UART_selectUART(UART0);
    SPI_updateRegister(CLKSRC_REG, CLKSRC_PLLBYP_BIT, SET_BIT);
    SPI_updateRegister(CLKSRC_REG, CLKSRC_PLL_BIT, CLR_BIT);
}

/*** Interrupt configuration ***/

//Enable interrupts - given the specific IRQ interrupt type to enable
void MAX14830 :: UART_IRQEnableInterrupt(IRQ_Interrupt interrupt) {
    SPI_updateRegister(IRQEN_REG, interrupt, SET_BIT);
}

//Disable interrupts - given the specific IRQ interrupt type to disable
void MAX14830 :: UART_IRQDisableInterrupt(IRQ_Interrupt interrupt) {
    SPI_updateRegister(IRQEN_REG, interrupt, CLR_BIT);
}

//Enable interrupts - given the specific LSR interrupt type to enable
void MAX14830 :: UART_LSREnableInterrupt(LSR_Interrupt interrupt) {
    SPI_updateRegister(LSRINTEN_REG, interrupt, SET_BIT);
}

//Disable interrupts - given the specific LSR interrupt type to disable
void MAX14830 :: UART_LSRDisableInterrupt(LSR_Interrupt interrupt) {
    SPI_updateRegister(LSRINTEN_REG, interrupt, CLR_BIT);
}

//Read ISR register value for pending interrupts
uint8_t MAX14830 :: UART_readInterrupts() {
    return SPI_readRegister(ISR_REG);
}

//Interrupt handler which calls registered interrupt callback function if interrupt occurs 
void MAX14830 :: UART_interruptHandler(uint8_t isrRegisterValue) {
    if(isrRegisterValue & LSR_INT) {
        LSRHandler();
    }
    else {
        DummyHandler();
    }
}

//Function to register LSR interrupt handler callback function
void MAX14830 :: UART_registerLSRInterruptHandler(void (*LSRInterruptHandler)(void)) {
    LSRHandler = LSRInterruptHandler;
}

//Function to register Dummy interrupt handler callback function
void MAX14830 :: UART_registerDummyHandler(void (*TestInterruptHandler)(void)) {
    DummyHandler = TestInterruptHandler;
}

/*** Baudrate configuration ***/

//Set baudrate and manage automatic 1x/2x/4x mode selection based on the gievn baudrate
void MAX14830 :: UART_setBaudrate(uint32_t baudrate) {
    uint16_t divide =  clockFreq / baudrate;
    uint16_t clockFactor;
    
    UART_disableClock();

    if (divide < 8) {
		// Mode 4x
		SPI_updateRegister(BRGCFG_REG, BRGCFG_4XMODE_BIT, SET_BIT);
        SPI_updateRegister(BRGCFG_REG, BRGCFG_2XMODE_BIT, CLR_BIT);
        clockFactor = 4;
	} else if (divide < 16) {
		// Mode 2x
		SPI_updateRegister(BRGCFG_REG, BRGCFG_4XMODE_BIT, CLR_BIT);
        SPI_updateRegister(BRGCFG_REG, BRGCFG_2XMODE_BIT, SET_BIT);
        clockFactor = 8;
	}
    else {
        // Mode 1x 
        SPI_updateRegister(BRGCFG_REG, BRGCFG_4XMODE_BIT, CLR_BIT);
        SPI_updateRegister(BRGCFG_REG, BRGCFG_2XMODE_BIT, CLR_BIT);
        clockFactor = 16;
    }

    divide = divide / clockFactor;
	SPI_writeRegister(DIVLSB_REG, divide);
	SPI_writeRegister(DIVMSB_REG, divide >> 8);
	UART_enableClock();
}


/*** UART modes ***/

void MAX14830 :: UART_enableAutoTransceiverControl() {
    SPI_updateRegister(MODE1_REG, MODE1_TRNSCVCTRL_BIT , SET_BIT);
}

void MAX14830 :: UART_disableAutoTransceiverControl() {
    SPI_updateRegister(MODE1_REG, MODE1_TRNSCVCTRL_BIT , CLR_BIT);
}

//Enable loopback (internally shorts tx to rx and cts to rts)
void MAX14830 :: UART_enableLoopback() {
    SPI_updateRegister(MODE2_REG, MODE2_LOOPBACK_BIT, SET_BIT);
}

//Disable loopback
void MAX14830 :: UART_disableLoopback() {
    SPI_updateRegister(MODE2_REG, MODE2_LOOPBACK_BIT, CLR_BIT);
}

//Enables transmitter of active UART
void MAX14830 :: UART_enableTransmitter() {
    SPI_updateRegister(MODE1_REG, MODE1_TXDIS_BIT, CLR_BIT);
}

//Disables transmitter of active UART
void MAX14830 :: UART_disableTransmitter() {
    SPI_updateRegister(MODE1_REG, MODE1_TXDIS_BIT, SET_BIT);
}

//Enables receiver of active UART
void MAX14830 :: UART_enableReceiver() {
    SPI_updateRegister(MODE1_REG, MODE1_RXDIS_BIT, CLR_BIT);
}

//Disables receiver of active UART
void MAX14830 :: UART_disableReceiver() {
    SPI_updateRegister(MODE1_REG, MODE1_RXDIS_BIT, SET_BIT);
}

/*** UART data modes ***/

//Set number of data bits in each UART message (data length)
void MAX14830 :: UART_setDataLength(uint8_t dataLength) {
    if(dataLength == 5) {
        SPI_updateRegister(LCR_REG, (LCR_LENGTH0_BIT | LCR_LENGTH1_BIT), CLR_BIT);
    }
    else if(dataLength == 6) {
        SPI_updateRegister(LCR_REG, LCR_LENGTH0_BIT, SET_BIT);
        SPI_updateRegister(LCR_REG, LCR_LENGTH1_BIT, CLR_BIT);
    }
    else if(dataLength == 7) {
        SPI_updateRegister(LCR_REG, LCR_LENGTH0_BIT, CLR_BIT);
        SPI_updateRegister(LCR_REG, LCR_LENGTH1_BIT, SET_BIT);
    }
    else if(dataLength == 8) {
        SPI_updateRegister(LCR_REG, (LCR_LENGTH0_BIT | LCR_LENGTH1_BIT), SET_BIT);
    }
    else {
        //Invalid data length
    }
}

//Set number of stop bits in each UART message (data length)
void MAX14830 :: UART_setStopBits(uint8_t stopBits) {
    uint8_t dataLength = (SPI_readRegister(LCR_REG) & 3) + 5;
    if(stopBits == 1 && dataLength > 5) {
        SPI_updateRegister(LCR_REG, LCR_STOPLEN_BIT, CLR_BIT);
    }
    else if(stopBits == 1 && dataLength == 5) {
        SPI_updateRegister(LCR_REG, LCR_STOPLEN_BIT, SET_BIT);
    }
    else if(stopBits == 2 && dataLength > 5) {
        SPI_updateRegister(LCR_REG, LCR_STOPLEN_BIT, SET_BIT);
    }
    else {
        //Invalid Stop bits
    }
}

//Enables parity and sets even parity for UART messages for the active UART
void MAX14830 :: UART_setEvenParity() {
    SPI_updateRegister(LCR_REG, LCR_PARITY_BIT | LCR_EVENPARITY_BIT, SET_BIT);
}

//Enables parity and sets odd parity for UART messages for the active UART
void MAX14830 :: UART_setOddParity() {
    SPI_updateRegister(LCR_REG, LCR_PARITY_BIT, SET_BIT);
    SPI_updateRegister(LCR_REG, LCR_EVENPARITY_BIT, CLR_BIT);
}

//Disable parity for UART messages on the active UART
void MAX14830 :: UART_disableParity() {
    SPI_updateRegister(LCR_REG, LCR_PARITY_BIT, CLR_BIT);
}

/*** UART Timing ***/

void MAX14830 :: UART_setReceiverTimeout(uint8_t timeout) {
    SPI_writeRegister(RXTIMEOUT_REG, timeout);
}

void MAX14830 :: UART_setSetupTime(uint8_t setupTime) {
    setupTime = (setupTime << 4) & 0xF0;
    SPI_updateRegister(HDPLXDELAY_REG, setupTime, SET_BIT);
}

void MAX14830 :: UART_setHoldTime(uint8_t holdTime) {
    holdTime = holdTime & 0x0F;
    SPI_updateRegister(HDPLXDELAY_REG, holdTime, SET_BIT);
}

/*** FIFO Control ***/

//Reset and clear both transmit and receive FIFOs for the active UART
void MAX14830 :: UART_resetFIFOs() {
    SPI_updateRegister(MODE2_REG, MODE2_FIFORST_BIT, SET_BIT);
    SPI_updateRegister(MODE2_REG, MODE2_FIFORST_BIT, CLR_BIT);
}

//Get receive FIFO level of given UART module
uint8_t MAX14830 :: UART_getRxFIFOLevel(UART_module uart) {
    uint8_t addr = getUARTBitmask(uart) | RXFIFOLVL_REG;
    addr &= 0x7F;
    uint8_t rxBuffer[] = {0x00};
    digitalWrite(spi_ss_pin, LOW);
    SPI.transfer(addr);
    SPI.transfer(NULL, rxBuffer, 1, NULL);
    digitalWrite(spi_ss_pin, HIGH);
    return rxBuffer[0];
}

//Get receive FIFO level of given UART module
uint8_t MAX14830 :: UART_getTxFIFOLevel(UART_module uart) {
    uint8_t addr = getUARTBitmask(uart) | TXFIFOLVL_REG;
    addr &= 0x7F;
    uint8_t rxBuffer[] = {0x00};
    digitalWrite(spi_ss_pin, LOW);
    SPI.transfer(addr);
    SPI.transfer(NULL, rxBuffer, 1, NULL);
    digitalWrite(spi_ss_pin, HIGH);
    return rxBuffer[0];
}

//Flow control level for receive FIFO halt
void MAX14830 :: UART_setTxFIFOHaltReceiveLvl(uint8_t txFIFOHaltLvl_x8) {
    txFIFOHaltLvl_x8 = txFIFOHaltLvl_x8 & 0x0F;
    SPI_updateRegister(FLOWLVL_REG, txFIFOHaltLvl_x8, SET_BIT);
}

//Flow control level for receive FIFO resume
void MAX14830 :: UART_setRxFIFOResumeReceiveLvl(uint8_t rxFIFOResumeLvl_x8) {
    rxFIFOResumeLvl_x8 = (rxFIFOResumeLvl_x8 << 4) & 0xF0;
    SPI_updateRegister(FLOWLVL_REG, rxFIFOResumeLvl_x8, SET_BIT);
}

//Interrupt trigger level for receive FIFO
void MAX14830 :: UART_setRxFIFOTrigLvl(uint8_t rxFIFOLvl_x8) {
    rxFIFOLvl_x8 = (rxFIFOLvl_x8 << 4) & 0xF0;
    SPI_updateRegister(FIFOTRIGLVL_REG, rxFIFOLvl_x8, SET_BIT);
}

//Interrupt trigger level for transmit FIFO
void MAX14830 :: UART_setTxFIFOTrigLvl(uint8_t txFIFOLvl_x8) {
    txFIFOLvl_x8 = txFIFOLvl_x8 & 0x0F;
    SPI_updateRegister(FIFOTRIGLVL_REG, txFIFOLvl_x8, SET_BIT);
}

/*** UART read/write ***/

//Write a byte of data to the given UART
void MAX14830 :: UART_writeByte(UART_module writeUART, uint8_t data) {
    uint8_t addr = getUARTBitmask(writeUART) | THR_REG;
    addr |= 0x80;
    uint8_t txBuffer[] = {addr, data};
    digitalWrite(spi_ss_pin, LOW);
    SPI.transfer(txBuffer, NULL, 2, NULL);
    digitalWrite(spi_ss_pin, HIGH);
}

//Write a series of contiguous bytes to the given UART (number of written read is the given size parameter)
void MAX14830 :: UART_writeBurst(UART_module writeUART, uint8_t *dataBuffer, uint8_t size) {
    uint8_t addr = getUARTBitmask(writeUART) | THR_REG;
    addr |= 0x80;
    digitalWrite(spi_ss_pin, LOW);
    SPI.transfer(addr);
    SPI.transfer(dataBuffer, NULL, size, NULL);
    digitalWrite(spi_ss_pin, HIGH);
}

//Read a byte of data from the given UART
uint8_t MAX14830 :: UART_readByte(UART_module readUART) {
    uint8_t addr = getUARTBitmask(readUART) | RHR_REG;
    addr &= 0x7F;
    uint8_t rxBuffer[] = {0x00};
    digitalWrite(spi_ss_pin, LOW);
    SPI.transfer(addr);
    SPI.transfer(NULL, rxBuffer, 1, NULL);
    digitalWrite(spi_ss_pin, HIGH);
    return rxBuffer[0];
}

//Reads a series of contiguous bytes from the given UART (number of bytes read is the given size parameter)
void MAX14830 :: UART_readBurst(UART_module readUART, uint8_t *dataBuffer, uint8_t size) {
    uint8_t addr = getUARTBitmask(readUART) | RHR_REG;
    addr &= 0x7F;
    digitalWrite(spi_ss_pin, LOW);
    SPI.transfer(addr);
    SPI.transfer(NULL, dataBuffer, size, NULL);
    digitalWrite(spi_ss_pin, HIGH);
}

//Get UART bitmask to apply to select the given UART
uint8_t MAX14830 :: getUARTBitmask(UART_module uart) {
    if(uart == UART0) {
        return 0x00;
    }
    else if(uart == UART1) {
        return 0x20;
    }
    else if(uart == UART2) {
        return 0x40;
    }
    else if(uart == UART3) {
        return 0x60;
    }
}


