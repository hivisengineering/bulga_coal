#include "Particle.h"

#define defaultSPI_ss D8            //Default SPI chip select pin
#define defaultClockFreq 3686400    //Default MAX14830 input clock/crystal frequency

//FIFO data registers
#define RHR_REG 0x00
#define THR_REG 0x00

//Interrupts registers
#define IRQEN_REG 0x01
#define ISR_REG 0x02
#define LSRINTEN_REG 0x03
#define LSR_REG 0x04
//Special Character Interrupt registers to be added
#define STSINTEN_REG 0x07
#define STSINT_REG 0x08

//UART Modes registers
#define MODE1_REG 0x09
#define MODE2_REG 0x0A
#define LCR_REG 0x0B
#define RXTIMEOUT_REG 0x0C
#define HDPLXDELAY_REG 0x0D
#define IRDA_REG 0x0E

//FIFOs control registers
#define FLOWLVL_REG 0x0F
#define FIFOTRIGLVL_REG 0x10
#define TXFIFOLVL_REG 0x11
#define RXFIFOLVL_REG 0x12

//Flow control registers
#define FLOWCTL_REG 0x13
//Software flow control XON,OFF registers to be added

//GPIO conf registers to be added

//Clock configuration registers
#define PLLCONFIG_REG 0x1A
#define BRGCFG_REG 0x1B
#define DIVLSB_REG 0x1C
#define DIVMSB_REG 0x1D
#define CLKSRC_REG 0x1E

//Global registers
#define GLOBALIRQ_REG 0x1F
#define GLOBLCMD_REG 0x1F

//Synchronization registers
#define TXSYNC_REG 0x20
//sync delay registers to be added

//Timer registers to be added

//Revision ID register
#define REVID_REG 0x25

/* IRQ register bits */
#define IRQ_LSR_BIT		(1 << 0) /* LSR interrupt */
#define IRQ_SPCHR_BIT		(1 << 1) /* Special char interrupt */
#define IRQ_STS_BIT		(1 << 2) /* Status interrupt */
#define IRQ_RXFIFO_BIT		(1 << 3) /* RX FIFO interrupt */
#define IRQ_TXFIFO_BIT		(1 << 4) /* TX FIFO interrupt */
#define IRQ_TXEMPTY_BIT		(1 << 5) /* TX FIFO empty interrupt */
#define IRQ_RXEMPTY_BIT		(1 << 6) /* RX FIFO empty interrupt */
#define IRQ_CTS_BIT		(1 << 7) /* CTS interrupt */

/* LSR register bits */
#define LSR_RXTO_BIT		(1 << 0) /* RX timeout */
#define LSR_RXOVR_BIT		(1 << 1) /* RX overrun */
#define LSR_RXPAR_BIT		(1 << 2) /* RX parity error */
#define LSR_FRERR_BIT		(1 << 3) /* Frame error */
#define LSR_RXBRK_BIT		(1 << 4) /* RX break */
#define LSR_RXNOISE_BIT		(1 << 5) /* RX noise */
#define LSR_CTS_BIT		(1 << 7) /* CTS pin state */

/* Status register bits */
#define STS_GPIO0_BIT		(1 << 0) /* GPIO 0 interrupt */
#define STS_GPIO1_BIT		(1 << 1) /* GPIO 1 interrupt */
#define STS_GPIO2_BIT		(1 << 2) /* GPIO 2 interrupt */
#define STS_GPIO3_BIT		(1 << 3) /* GPIO 3 interrupt */
#define STS_CLKREADY_BIT	(1 << 5) /* Clock ready */
#define STS_SLEEP_BIT		(1 << 6) /* Sleep interrupt */

/* MODE1 register bits */
#define MODE1_RXDIS_BIT		(1 << 0) /* RX disable */
#define MODE1_TXDIS_BIT		(1 << 1) /* TX disable */
#define MODE1_TXHIZ_BIT		(1 << 2) /* TX pin three-state */
#define MODE1_RTSHIZ_BIT	(1 << 3) /* RTS pin three-state */
#define MODE1_TRNSCVCTRL_BIT	(1 << 4) /* Transceiver ctrl enable */
#define MODE1_FORCESLEEP_BIT	(1 << 5) /* Force sleep mode */
#define MODE1_AUTOSLEEP_BIT	(1 << 6) /* Auto sleep enable */
#define MODE1_IRQSEL_BIT	(1 << 7) /* IRQ pin enable */

/* MODE2 register bits */
#define MODE2_RST_BIT		(1 << 0) /* Chip reset */
#define MODE2_FIFORST_BIT	(1 << 1) /* FIFO reset */
#define MODE2_RXTRIGINV_BIT	(1 << 2) /* RX FIFO INT invert */
#define MODE2_RXEMPTINV_BIT	(1 << 3) /* RX FIFO empty INT invert */
#define MODE2_SPCHR_BIT		(1 << 4) /* Special chr detect enable */
#define MODE2_LOOPBACK_BIT	(1 << 5) /* Internal loopback enable */
#define MODE2_MULTIDROP_BIT	(1 << 6) /* 9-bit multidrop enable */
#define MODE2_ECHOSUPR_BIT	(1 << 7) /* ECHO suppression enable */

/* LCR register bits */
#define LCR_LENGTH0_BIT		(1 << 0) /* Word length bit 0 */
#define LCR_LENGTH1_BIT		(1 << 1) /* Word length bit 1 */
						  /*
						  * Word length bits table:
						  * 00 -> 5 bit words
						  * 01 -> 6 bit words
						  * 10 -> 7 bit words
						  * 11 -> 8 bit words
						  */
#define LCR_STOPLEN_BIT		(1 << 2) /* STOP length bit */
						  /*
						  * STOP length bit table:
						  * 0 -> 1 stop bit
						  * 1 -> 1-1.5 stop bits if
						  *      word length is 5,
						  *      2 stop bits otherwise
						  */
#define LCR_PARITY_BIT		(1 << 3) /* Parity bit enable */
#define LCR_EVENPARITY_BIT	(1 << 4) /* Even parity bit enable */
#define LCR_FORCEPARITY_BIT	(1 << 5) /* 9-bit multidrop parity */
#define LCR_TXBREAK_BIT		(1 << 6) /* TX break enable */
#define LCR_RTS_BIT		(1 << 7) /* RTS pin control */

/* Baud rate generator configuration register bits */
#define BRGCFG_2XMODE_BIT	(1 << 4) /* Double baud rate */
#define BRGCFG_4XMODE_BIT	(1 << 5) /* Quadruple baud rate */
#define BRGCFG_CLKDISABL_BIT	(1 << 6) /* Disable UART clock  */

/* Clock source register bits */
#define CLKSRC_CRYST_BIT	(1 << 1) /* Crystal osc enable */
#define CLKSRC_PLL_BIT		(1 << 2) /* PLL enable */
#define CLKSRC_PLLBYP_BIT	(1 << 3) /* PLL bypass */
#define CLKSRC_EXTCLK_BIT	(1 << 4) /* External clock enable */
#define CLKSRC_CLK2RTS_BIT	(1 << 7) /* Baud clk to RTS pin */

/* PLL configuration register bits */
#define PLLCFG_PLLFACTOR1	(1 << 7) /* PLL multiplication factor */
#define PLLCFG_PLLFACTOR0   (1 << 6) /* PLL multiplication factor */
#define PLLCFG_PREDIV5	    (1 << 5) /* PLL pre-divider */
#define PLLCFG_PREDIV4	    (1 << 4) /* PLL pre-divider */
#define PLLCFG_PREDIV3	    (1 << 3) /* PLL pre-divider */
#define PLLCFG_PREDIV2	    (1 << 2) /* PLL pre-divider */
#define PLLCFG_PREDIV1	    (1 << 1) /* PLL pre-divider */
#define PLLCFG_PREDIV0	    (1 << 0) /* PLL pre-divider */


// UART modules (for selecting active UART module)
typedef enum {
    UART0,
    UART1,
    UART2,
    UART3
} UART_module;

// Used internally for updating individual/collection of bits in a register
typedef enum {
    CLR_BIT,
    SET_BIT
} RegisterUpdateType;

// List of IRQ interrupts - can be used to enable/disable/check corresponding IRQ interrupt
typedef enum {
    LSR_INT = (1 << 0),
    SPCHAR_INT = (1 << 1),
    STS_INT = (1 << 2),
    RFIFOTRIG_INT = (1 << 3),
    TFIFOTRIG_INT = (1 << 4),
    TFIFOEMPTY_INT = (1 << 5),
    RFIFOEMPTY_INT = (1 << 6),
    CTS_INT = (1 << 7)
} IRQ_Interrupt;

// List of LSR interrupts - can be used to enable/disable/check corresponding LSR interrupt
// LSR interrupts are line and status interrupts for detecting line noise, parity error etc. 
typedef enum {
    RTIMEOUT_INT = (1 << 0),
    ROVERRUN_INT = (1 << 1),
    PARITY_INT = (1 << 2),
    FRAMEERR_INT = (1 << 3),
    RBREAK_INT = (1 << 4),
    RXNOISE_INT = (1 << 5)
} LSR_Interrupt;


/* MAX14830 object */
class MAX14830 {
    public:
    int spi_ss_pin;
    unsigned int clockFreq;
    UART_module active_UART_module = UART0;
    
    //default constructor initializes MAX14830 object with default chip select and clock freuquency
    MAX14830() {
        spi_ss_pin = defaultSPI_ss;
        clockFreq = defaultClockFreq;
        MAX14830_init();
    }
    
    //parameterized constructor initializes MAX14830 object with custom chip select and clock freuquency
    MAX14830(int new_spi_ss, unsigned int newClockFreq) {
        spi_ss_pin = new_spi_ss;
        clockFreq = newClockFreq;
        MAX14830_init();
    }
    
    //Set active UART module
    void UART_selectUART(UART_module uart) {
        active_UART_module = uart;
    }

    /***************************************************/
    /************** Function Declarations **************/
    /***************************************************/

    //Initialization functions
    void MAX14830_init(void);
    void UART_resetAll(void);

    //SPI functions
    void SPI_setup(void);
    uint8_t SPI_readRegister(uint8_t addr);
    void SPI_writeRegister(uint8_t addr, uint8_t data);
    void SPI_updateRegister(uint8_t addr, uint8_t data, RegisterUpdateType updateType);
    bool SPI_checkBit(uint8_t addr, uint8_t bitmask);
    
    //Clock configuration
    void UART_disableClock(void);
    void UART_enableClock(void);
    bool UART_checkClockReady(void);
    void UART_enablePLL(void);
    void UART_setPLLPredivider(uint8_t divider);
    void UART_setPLLMultiplier(uint8_t multiplier);
    void UART_disablePLL(void);

    //Interrupt configuration
    uint8_t UART_readInterrupts(void);
    void UART_IRQEnableInterrupt(IRQ_Interrupt interrupt);
    void UART_IRQDisableInterrupt(IRQ_Interrupt interrupt);
    void UART_LSREnableInterrupt(LSR_Interrupt interrupt);
    void UART_LSRDisableInterrupt(LSR_Interrupt interrupt);
    void UART_interruptHandler(uint8_t isrRegisterValue);
    void UART_registerLSRInterruptHandler(void (*LSRInterruptHandler)(void));
    void UART_registerDummyHandler(void (*TestInterruptHandler)(void));

    //Baudrate configuration
    void UART_setBaudrate(uint32_t baudrate); // Set to regular 1x baudrate when called
    
    //UART modes
    void UART_enableAutoTransceiverControl(void);
    void UART_disableAutoTransceiverControl(void);
    void UART_enableLoopback(void);
    void UART_disableLoopback(void);
    void UART_enableTransmitter(void);
    void UART_disableTransmitter(void);
    void UART_enableReceiver(void);
    void UART_disableReceiver(void);

    //UART data modes
    void UART_setDataLength(uint8_t dataLength);
    void UART_setStopBits(uint8_t stopBits);
    void UART_setEvenParity(void);
    void UART_setOddParity(void);
    void UART_disableParity(void);

    //UART timing
    void UART_setReceiverTimeout(uint8_t timeout);
    void UART_setSetupTime(uint8_t setupTime);
    void UART_setHoldTime(uint8_t holdTime);

    //FIFO control
    void UART_resetFIFOs(void);
    uint8_t UART_getRxFIFOLevel(UART_module uart);
    uint8_t UART_getTxFIFOLevel(UART_module uart);
    void UART_setTxFIFOHaltReceiveLvl(uint8_t txFIFOHaltLvl_x8);
    void UART_setRxFIFOResumeReceiveLvl(uint8_t rxFIFOResumeLvl_x8);
    void UART_setRxFIFOTrigLvl(uint8_t rxFIFOLvl_x8);
    void UART_setTxFIFOTrigLvl(uint8_t txFIFOLvl_x8);   
    
    //UART read/write
    void UART_writeByte(UART_module writeUART, uint8_t data);
    void UART_writeBurst(UART_module writeUART, uint8_t *dataBuffer, uint8_t size);
    uint8_t UART_readByte(UART_module readUART);
    void UART_readBurst(UART_module readUART, uint8_t *dataBuffer, uint8_t size);
        
    private:
    //Interrupt Handler Function Pointers
    void (*LSRHandler)(void);
    void (*DummyHandler)(void);

    //Get UART bitmask to apply to select the given UART
    uint8_t getUARTBitmask(UART_module uart);
};