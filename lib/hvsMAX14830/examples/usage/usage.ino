/*
 * Project MAX14830_Library
 * Description:
 * Author:
 * Date:
 */

// This #include statement was automatically added by the Particle IDE.
#include "MAX14830.h"

SYSTEM_THREAD(ENABLED);

bool runOnce = true;

MAX14830 SPItoUART;

void setupUART() {
    SPItoUART.UART_selectUART(UART1);
    SPItoUART.UART_setBaudrate(115200);
    SPItoUART.UART_setDataLength(8);
    SPItoUART.UART_setStopBits(1);
    SPItoUART.UART_enableAutoTransceiverControl();
    SPItoUART.UART_resetFIFOs();

    setupInterrupts();
}

void setupInterrupts() {
    Particle.publish("Enabling LSR Interrupt", "Success");
    delay(500);
    SPItoUART.UART_IRQEnableInterrupt(LSR_INT);
    SPItoUART.UART_LSREnableInterrupt(RTIMEOUT_INT);
    SPItoUART.UART_LSREnableInterrupt(ROVERRUN_INT);
    SPItoUART.UART_LSREnableInterrupt(PARITY_INT);
    SPItoUART.UART_LSREnableInterrupt(FRAMEERR_INT);
    SPItoUART.UART_LSREnableInterrupt(RBREAK_INT);
    SPItoUART.UART_LSREnableInterrupt(RXNOISE_INT);
    
    SPItoUART.UART_registerLSRInterruptHandler(ErrorInterruptHandler);
    SPItoUART.UART_registerDummyHandler(TestInterruptHandler);

    Particle.publish("UART1 Interrupt Register", String::format("%d", SPItoUART.UART_readInterrupts()));
    delay(500);
}

void action() {
    setupUART();

    //check clock ready and wait till clock ready
    while(!SPItoUART.UART_checkClockReady()) {
        Particle.publish("Waiting for clock ready", String::format("%s", SPItoUART.UART_checkClockReady() ? "true" : "false"));
        delay(1000);
    }
    Particle.publish("Clock ready!", "Success");
    delay(500);

    //check clock ready and wait till clock ready
    if(SPItoUART.SPI_checkBit(MODE2_REG, MODE2_LOOPBACK_BIT)) {
        Particle.publish("Loopback", "Enabled");
        delay(500);
    }
    else {
        Particle.publish("Loopback", "Disabled");
        delay(500);
    }

    Particle.publish("MAX14830 ready for data transmission");
    delay(500);
}

int CloudWriteCharUART1(String in) {
    Particle.publish("Writing to UART1 = ", String::format("%c", in.charAt(0)));
    delay(500);
    SPItoUART.UART_writeByte(UART1, in.charAt(0));
    return 0;
}

int CloudWriteBurst(String in) {
    byte txStr[in.length()];
    in.getBytes(txStr, in.length() + 1);
    SPItoUART.UART_writeBurst(UART1, (uint8_t *) txStr, in.length() + 1);
    Particle.publish("UART1 write burst", "Success");
    delay(500);
    return 0;
}

int CloudReadCharUART1(String in) {
    Particle.publish("Reading from UART1 = ", String::format("%c", SPItoUART.UART_readByte(UART1)));
    delay(500);
    return 0;
}

int CloudReadBurst(String in) {
    Particle.publish("UART1 RX_FIFO_LVL before read = ", String::format("%d", SPItoUART.UART_getRxFIFOLevel(UART1)));
    delay(500);
    uint8_t readBufferLevel = SPItoUART.UART_getRxFIFOLevel(UART1);
    uint8_t readBurst[128];
    SPItoUART.UART_readBurst(UART1, readBurst, readBufferLevel);
    char *rxBurst = (char *) readBurst;
    Particle.publish("Read Burst :: ", String::format("%s", rxBurst));
    Particle.publish("UART1 read burst = ", "Success");
    delay(500);
    Particle.publish("UART1 RX_FIFO_LVL after read = ", String::format("%d", SPItoUART.UART_getRxFIFOLevel(UART1)));
    delay(500);
    return 0;
}

int CloudBaudrate(String in) {
    int baudrate = in.toInt();
    Particle.publish("Setting buadrate = ", String::format("%d", baudrate));
    delay(500);
    SPItoUART.UART_setBaudrate(baudrate);
    return 0;
}

int CloudEnableLoopback(String in) {
    Particle.publish("Enabling Loopback");
    delay(500);
    SPItoUART.UART_enableLoopback();
    return 0;
}

int CloudDisableLoopback(String in) {
    Particle.publish("Disabling Loopback");
    delay(500);
    SPItoUART.UART_disableLoopback();
    return 0;
}

void ErrorInterruptHandler() {
    Particle.publish("Error Interrupt Handler");
    Log.info("Error Interrupt Handler");
    delay(500);
}

void TestInterruptHandler() {
    Particle.publish("Test Interrupt Handler");
    Log.info("Test Interrupt Handler");
    delay(500);
}

void setup() {
    Particle.function("SetBaudrate", CloudBaudrate);
    Particle.function("WriteCharUART1", CloudWriteCharUART1);
    Particle.function("ReadCharUART1", CloudReadCharUART1);
    Particle.function("WriteBurst", CloudWriteBurst);
    Particle.function("ReadBurst", CloudReadBurst);
    Particle.function("EnableLoopback", CloudEnableLoopback);
    Particle.function("DisableLoopback", CloudDisableLoopback);
}

void loop() {
    if (Particle.connected() == false) {
        Particle.connect();
        while (!Particle.connected()){
            //Log.info("connecting to cloud");
        }
    }
    
    while(runOnce) {
        action();
        runOnce = false;
    }
}