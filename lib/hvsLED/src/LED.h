// Version 1.0 - 16.MAR.20

#ifndef LED_H
#define LED_H

class LEDClass{

    private:
        int _pin; //use _variable format for private variables
        int _flashrate;
        int _PWMdim;
    	int _PWMbright;
        unsigned long _previousMillis = 0;
        unsigned long _currentMillis;

    public:
        bool state;
        LEDClass(int in1, int in2, int in3);
        void flash(int PWM);
        void solidOn(int PWM);
        void flashHard();
        void off();
        void updateFlashRate(int NewFlashRate);
};

#endif
