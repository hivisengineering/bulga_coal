// Version 1.0 - 16.MAR.20

#include "LED.h"
#include "Particle.h"

LEDClass::LEDClass(int in1, int in2, int in3)
{
	_pin = in1;
	pinMode(_pin, OUTPUT);
	state = in2;
	_flashrate = in3;
}

void LEDClass::flash(int PWM) //input the brightness to write to the LED
{
	_currentMillis = millis();
	if (_currentMillis - _previousMillis >= _flashrate) {
		_previousMillis = _currentMillis;
		if (state == LOW) {
		  state = HIGH;
		  analogWrite(_pin, PWM);
		} else {
		  state = LOW;
		  digitalWrite(_pin, 0);
		}
	}
}

void LEDClass::solidOn(int PWM)
{
	analogWrite(_pin, PWM);
}

void LEDClass::flashHard()
{
	_currentMillis = millis();
	if (_currentMillis - _previousMillis >= _flashrate) {
		_previousMillis = _currentMillis;
		if (state == LOW) {
		  state = HIGH;
		  digitalWrite(_pin, 1);
		} else {
		  state = LOW;
		  digitalWrite(_pin, 0);
		}
	}
}

void LEDClass::off(){
	digitalWrite(_pin, 0);
}

void LEDClass::updateFlashRate(int NewFlashRate){
	_flashrate = NewFlashRate;
}
