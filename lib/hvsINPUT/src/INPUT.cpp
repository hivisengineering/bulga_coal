// Version 1.0 - 16.11.20

#include "INPUT.h"
#include "Particle.h"

//Class Constructor
Input::Input(int pin, int defaultstate) {
  this->pin = pin;
  pinMode (this->pin, INPUT);
  this->state = defaultstate;
}

bool Input::getState() {
  return(this->state);
}

void Input::check() {
  if (this->checkCount++ > this->checkLimit) {
    if (this->GPIOcount > this->debouncehighlimit) this->state = 1;
    if (this->GPIOcount < this->debouncelowlimit) this->state = 0;
    this->GPIOcount=0;
    this->checkCount=0;
  }
  if (digitalRead (this->pin)) this->GPIOcount++;
}