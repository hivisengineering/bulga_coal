// Version 1.0 - 17.11.20 J.Lawlor

#ifndef INPUT_H
#define INPUT_H

//change this if you want to log to SDCard or Remote Server or timestamp etc
class Input {
  private:
    const int debouncelowlimit = 3;
    const int debouncehighlimit = 12;
    const int checkLimit = 15;
    int checkCount=0;
    int GPIOcount=0;
    int pin;

  public:
    int state;
    Input(int pin);
    void check();
    bool getState();
    Input(int pin, int defaultstate=0);
};

#endif
