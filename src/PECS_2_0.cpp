/******************************************************/
//       THIS IS A GENERATED FILE - DO NOT EDIT       //
/******************************************************/

#line 1 "c:/Users/jaredlawlor/OneDriveHVS/Documents/LEDMagic/PECS_2_0/src/PECS_2_0.ino"
/*
 * Project PECS_2_0
 * version 0.0.1
 * Description: Base code for functionality of the Hi-Vis Group PECS 2.0 PCB
 * Author: J.Lawlor
 * Date: 01/09/2021
 */

//#pragma PARTICLE_NO_PREPROCESSOR
//The below are to always be included
#include "Particle.h"
#include "MAX14830.h"
#include "LED.h"
#include "MCP3021-RK.h"
#include "MCP23017-RK.h"

//Optional includes
#include "MPU6050.h"
#include "sunset.h"
#include "SparkTime.h"

void handler(const char *topic, const char *data);
void setupUARTs();
void setupInterrupts();
void runOnce();
void ErrorInterruptHandler();
void TestInterruptHandler();
void startEEPROM();
int resetEEPROM(String Reset);
void calMode(String TriggerType, String Data);
int calibration(String Mode);
int forceOn(String force);
int power335(String pwr);
int setHST(String HST);
int setSignOnTime(String on);
int setFlashRate1(String flash);
int setFlashRate2(String flash);
int CloudBaudrate(String in);
void check335();
void publishData();
void checkAnalogInputs();
void burstWriteUART(int uart, String in);
void setup();
void loop();
#line 22 "c:/Users/jaredlawlor/OneDriveHVS/Documents/LEDMagic/PECS_2_0/src/PECS_2_0.ino"
SYSTEM_THREAD(ENABLED);

char DeviceName[32]            = "";
void handler(const char *topic, const char *data) {
  strncpy(DeviceName, data, sizeof(DeviceName)-1);
  Serial.printlnf("received %s: %s", topic, DeviceName);
}

String version = "PECS_2_0_080921_JL_2";

SerialLogHandler logHandler;
Serial1LogHandler logHandler2(115200);

// MPU variables:
MPU6050 accelgyro;
int16_t ax, ay, az;
int16_t gx, gy, gz;

//Expansion I/O initialisation
MCP23017 gpio(Wire, 0);
MCP3021 adc(Wire);

//SPI > UART bridge initialisation
MAX14830 SPItoUART;

String ReadString335;

/* Project Variables */
int HighSpeedThreshold          = 20;
int pinLantern1                 = D4;
int pinLantern2                 = D5;
//int pin335                      = gpio.mapPin(); // needs to be updated to the EIO pin GPA2
//int pinExLed1                   = gpio.PIN_2;
int Speed 				        = 0;

/* LED Variables */
int FlashRate1                  = 500;
int FlashRate2                  = 350;
int SignOnTime                  = 3000;        // Time to hold the 335 trigger on for
unsigned long SignOnTimer       = 0;
int CurrPWM                     = 255;
int PWMdim                      = 60;
int PWMbright                   = 255;
int LuxVal;
int LuxLow                      = 249;         // Change this value to change the ambient light level crossover at which the LED's will dim
int LuxHigh                     = 250;

/* Sign Data Variables */
int pinVoltageSense             = A0;
int pinLDR                      = A5;
float Vdivider                  = 4.43;

/* Particle Function Variables */
int ForceOn                     = 0;
int CalMode                     = 0;

/* Particle Variables */
double BattVolt             	= 0;
double EnclosureTemp            = 0;

/* Timing Functionality Variables */
long PublishTime               = 360000; //time for particle publish functon
unsigned long LastPublish      = 0;
long SenseReadTime             = 600000; //time for sense check
unsigned long LastSenseRead    = 0;
long CalPubTime                = 1100; //time for calibration publishes
unsigned long LastCalPub       = 0;
long CalOnTmer                 = 900000; //time to stay in cal mode
unsigned long LastCalOnTmer    = 0;
long ForceOnTime               = 30000; //time to keep the force flash on
unsigned long LastForceOnTimer = 0;

/* Sensor Variables */
bool Detect335                  = false;    // Is the 335 detecting a vehicle?

/* EEPROM Variables */
int EEPROMinit;
bool EEPROMnew; // Determine if the EEPROM is new i.e. needs to be initialised or not
int EEPROMdata[] = {54, FlashRate1, FlashRate2, SignOnTime, HighSpeedThreshold};

LEDClass LED_1(pinLantern1,0,FlashRate1);
LEDClass LED_2(pinLantern2,1,FlashRate2);

bool bootUp = true;

void setupUARTs() {
    SPItoUART.UART_selectUART(UART0);
    SPItoUART.UART_setBaudrate(115200);
    SPItoUART.UART_setDataLength(8);
    SPItoUART.UART_setStopBits(1);
    SPItoUART.UART_enableAutoTransceiverControl();
    SPItoUART.UART_resetFIFOs();
    
    SPItoUART.UART_selectUART(UART1);
    SPItoUART.UART_setBaudrate(115200);
    SPItoUART.UART_setDataLength(8);
    SPItoUART.UART_setStopBits(1);
    //SPItoUART.UART_enableAutoTransceiverControl();
    SPItoUART.UART_resetFIFOs();

    SPItoUART.UART_selectUART(UART2);
    SPItoUART.UART_setBaudrate(115200);
    SPItoUART.UART_setDataLength(8);
    SPItoUART.UART_setStopBits(1);
    SPItoUART.UART_enableAutoTransceiverControl();
    SPItoUART.UART_resetFIFOs();

    SPItoUART.UART_selectUART(UART3);
    SPItoUART.UART_setBaudrate(115200);
    SPItoUART.UART_setDataLength(8);
    SPItoUART.UART_setStopBits(1);
    SPItoUART.UART_enableAutoTransceiverControl();
    SPItoUART.UART_resetFIFOs();

    setupInterrupts();
}

void setupInterrupts() {
    SPItoUART.UART_IRQEnableInterrupt(LSR_INT);
    SPItoUART.UART_LSREnableInterrupt(RTIMEOUT_INT);
    SPItoUART.UART_LSREnableInterrupt(ROVERRUN_INT);
    SPItoUART.UART_LSREnableInterrupt(PARITY_INT);
    SPItoUART.UART_LSREnableInterrupt(FRAMEERR_INT);
    SPItoUART.UART_LSREnableInterrupt(RBREAK_INT);
    SPItoUART.UART_LSREnableInterrupt(RXNOISE_INT);
    
    SPItoUART.UART_registerLSRInterruptHandler(ErrorInterruptHandler);
    SPItoUART.UART_registerDummyHandler(TestInterruptHandler);

    Log.info("UART1 Interrupt Register", String::format("%d", SPItoUART.UART_readInterrupts()));
    delay(500);
}

void runOnce() {
    setupUARTs();
    //check clock ready and wait till clock ready
    while(!SPItoUART.UART_checkClockReady()) {
        Log.info("Waiting for clock ready", String::format("%s", SPItoUART.UART_checkClockReady() ? "true" : "false"));
        delay(1000);
    }
    Log.info("Clock ready!", "Success");
    delay(500);

    Log.info("MAX14830 ready for data transmission");
    delay(500);
}

void ErrorInterruptHandler() {
    Particle.publish("Error Interrupt Handler");
    Log.info("Error Interrupt Handler");
    delay(500);
}

void TestInterruptHandler() {
    Particle.publish("Test Interrupt Handler");
    Log.info("Test Interrupt Handler");
    delay(500);
}

void startEEPROM(){
    // See if EEPROM has been initalised
    EEPROM.get(0,EEPROMinit);
    if (EEPROMinit != 54){  // Random number used to identify if EEPROM has been initalised
        int arrLen = sizeof(EEPROMdata) / sizeof(int);
        int addr = 0;
        for (int i=0; i<arrLen; i++){
            EEPROM.put(addr,EEPROMdata[i]);
            addr += sizeof(int);
        }
        EEPROMnew = 1;
    }
    else if (EEPROMinit == 54){
        EEPROM.get(4,FlashRate1); // Addresses increment by 4 - sizeof(int)
        EEPROM.get(8,FlashRate2); // Addresses increment by 4 - sizeof(int)
        EEPROM.get(12,SignOnTime); // Addresses increment by 4 - sizeof(int)
        EEPROM.get(16,HighSpeedThreshold); // Addresses increment by 4 - sizeof(int)
        EEPROMnew = 0;
        LED_1.updateFlashRate(FlashRate1);
        LED_2.updateFlashRate(FlashRate2);
    }
}

int resetEEPROM(String Reset){
    bool ResetEEPROM = atoi(Reset);
    if (ResetEEPROM == 1){
        EEPROM.clear();
        if(Particle.connected()){
    	       Particle.publish("Clearing EEPROM");
        }
    }
    return ResetEEPROM;
}

void calMode(String TriggerType, String Data) {
	if (millis() - LastCalOnTmer < CalOnTmer){
	    if (Particle.connected() && CalMode == 1 && ((millis() - LastCalPub >= CalPubTime))){
	        Particle.publish(TriggerType, Data);
			CalMode = 1;
	        LastCalPub = millis();
	    }
	}
	else CalMode = 0;
}

int calibration(String Mode){													// setting device to calibration mode will report device functional status to Console for debugging
    CalMode = atoi(Mode);
    if (CalMode == 1){
        LastCalOnTmer = millis();
        if(Particle.connected()){
    	       Particle.publish("Entering Cal Mode");
        }
    }
	else if (CalMode == 0 && Particle.connected()){
        Particle.publish("Exiting Cal Mode");
    }
    return CalMode;
}

int forceOn(String force){													    // Forces local activation of the sign to prove local functionality
    ForceOn = atoi(force);
    if (ForceOn == 1){
        LastForceOnTimer = millis();
    }
    if(Particle.connected()){
        Particle.publish("Force Flash", String(ForceOn));
    }
    return ForceOn;
}


int power335(String pwr){                                                       // Power the radar through on board MOSFET 
    int State335 = atoi(pwr);
    gpio.digitalWrite(2, State335);
    return State335;
}

int setHST(String HST){													        // Forces local activation of the sign to prove local functionality
    HighSpeedThreshold = atoi(HST);
    EEPROM.put(16,HighSpeedThreshold);
    return HighSpeedThreshold;
}

int setSignOnTime(String on){													// Sets the amount of time each trigger event will activate the sign for
    SignOnTime = atoi(on);
    EEPROM.put(12,SignOnTime);
    return SignOnTime;
}

int setFlashRate1(String flash){												// Sets the flash rate of light 1 via cloud function 
    FlashRate1 = atoi(flash);
    LED_1.updateFlashRate(atoi(flash));
    EEPROM.put(4,FlashRate1);
    return FlashRate1;
}

int setFlashRate2(String flash){												// Sets the flash rate of light 2 via cloud function
    FlashRate2 = atoi(flash);
    LED_2.updateFlashRate(atoi(flash));
    EEPROM.put(8,FlashRate2);
    return FlashRate2;
}

int CloudBaudrate(String in) {
    int baudrate = in.toInt();
    Particle.publish("Setting buadrate = ", String::format("%d", baudrate));
    delay(500);
    SPItoUART.UART_setBaudrate(baudrate);
    return 0;
}


void check335(){
    while (SPItoUART.UART_getRxFIFOLevel(UART1) > 0){
        char c = SPItoUART.UART_readByte(UART1);
        //delay(5);
        ReadString335 += c;
        //Log.info("ASCII Char = " + String(c));
        //Log.info("UART Byte = " + String(SPItoUART.UART_readByte(UART1)));
        //Log.info("FIFO LVL = " + String(SPItoUART.UART_getRxFIFOLevel(UART1)));
        if (c == '\r' && ReadString335.length() == 6){
            Log.info("here");
            Log.info(ReadString335);
            Speed = ReadString335.substring(2).toInt();
            if (Speed >= HighSpeedThreshold){
                Log.info(String(Speed));
                Detect335 = true;
                SignOnTimer = millis();
                calMode("Detect335", String(Speed));
            }
            ReadString335 = "";
        }

        else if (c == '\r' && ReadString335.length() > 6){
            Log.info("here 1");
            Log.info(ReadString335);
            ReadString335 = "";
        }

        else if (c == '\r' && ReadString335.length() < 6){
            Log.info("here 2");
            Log.info(ReadString335);
                ReadString335 = "";
            }

        else if (c == 'C'){
            if (Particle.connected()){
                Particle.publish("Activated",String(Speed));
                ReadString335 = "";
            }
        }
        delay(2);
    }
}

void publishData(){
    String JsonHealthString =
    String("{ \"Device\":\"" + String(DeviceName) + "\""
        + ",\"Battery_Voltage\":" + String(BattVolt,2)
        + ",\"Enclosure_Temp\":" + String(EnclosureTemp,2)
        + ",\"Light_Sensor\":" + String(float(LuxVal) /4095, 2)
        + ",\"version\":\"" + String(version) + "\""
        + "}");

    if (((millis() - LastPublish >= PublishTime) && (Particle.connected())) || bootUp) {
        Particle.publishVitals();
        Particle.publish("particle/device/name");
        Particle.publish("Health", JsonHealthString, PRIVATE);
        LastPublish = millis();
    }
}

void checkAnalogInputs(){
    LuxVal = analogRead(pinLDR);
    BattVolt = ((analogRead(pinVoltageSense)*3.3)/4095*Vdivider);
}

void burstWriteUART(int uart, String in){
    byte txStr[in.length()];
    in.getBytes(txStr, in.length() + 1);
    //Log.info(in);
    if(uart = 0){
        SPItoUART.UART_writeBurst(UART0, (uint8_t *) txStr, in.length() + 1);
    }
    else if (uart = 1){
        SPItoUART.UART_writeBurst(UART1, (uint8_t *) txStr, in.length() + 1);
    }
    else if (uart = 2){
        SPItoUART.UART_writeBurst(UART2, (uint8_t *) txStr, in.length() + 1);
    }
    else if (uart = 3){
        SPItoUART.UART_writeBurst(UART3, (uint8_t *) txStr, in.length() + 1);
    }
}

void setup() {
    Particle.subscribe("particle/device/name", handler);			// Master device that this slave will subscribe to and the string that needs to be publish
    Particle.publish("particle/device/name");

    Particle.function("SetBaudrate", CloudBaudrate);
    Particle.function("Calibration",calibration);
    Particle.function("High_Speed_Threshold", setHST);
    Particle.function("FlashRate1",setFlashRate1);
    Particle.function("FlashRate2",setFlashRate2);
    Particle.function("OnTime",setSignOnTime);
    Particle.function("Force_On",forceOn);
    Particle.function("335_Power",power335);
    Particle.function("Reset_EEPROM", resetEEPROM);

    Particle.variable("BattVolt",BattVolt);
    Particle.variable("HighSpeedThreshold",HighSpeedThreshold);
    Particle.variable("SoftVer", version);
    Serial.begin(9600);
    Wire.begin();
    
    pinMode(D6, OUTPUT); 
    pinMode(D7, OUTPUT);
    gpio.begin(); // start I2C wire comms with Expansion GPIO then initialise the GPIO
    gpio.pinMode(6, INPUT);     //SWITCH S5 (RIGHT)
    gpio.pinMode(7, INPUT);     //SWITCH S6 (LEFT)
    gpio.pinMode(12, OUTPUT);   //LED 1
    gpio.pinMode(13, OUTPUT);   //LED 2
    gpio.pinMode(14, OUTPUT);   //LED 3
    gpio.pinMode(1, OUTPUT);    //OUTPUT 5 
    gpio.pinMode(2, OUTPUT);    //OUTPUT 6

    gpio.digitalWrite(2, HIGH); //turn on power to radar at OUT6 on PCB

    startEEPROM();
}

void loop() {

    check335();
/*
    if(SPItoUART.UART_getRxFIFOLevel(UART1) > 4){
        uint8_t readBufferLevel = SPItoUART.UART_getRxFIFOLevel(UART1);
        uint8_t readBurst[128];
        SPItoUART.UART_readBurst(UART1, readBurst, readBufferLevel);
        char *rxBurst = (char *) readBurst;
        Log.info(String(rxBurst));
    }
*/
    //Particle.publish("Read Burst :: ", String::format("%s", rxBurst));
    //Particle.publish("UART1 read burst = ", "Success");
    //Particle.publish("UART1 RX_FIFO_LVL after read = ", String::format("%d", SPItoUART.UART_getRxFIFOLevel(UART1)));


    checkAnalogInputs();
    if(gpio.digitalRead(6) && gpio.digitalRead(7)){
        if (SPItoUART.UART_getTxFIFOLevel(UART1) < 127){
            burstWriteUART(1,   "*S031\r*S030\r*S031\r*S030\r");
            gpio.digitalWrite(12, HIGH);
            Log.info(String(SPItoUART.UART_getTxFIFOLevel(UART1)));
        }
    }
/*
    else if (gpio.digitalRead(7))  {
        burstWriteUART(1, "AGD?\r");
        gpio.digitalWrite(13, HIGH);
    }
 */   
    else {
        gpio.digitalWrite(12, LOW);
        gpio.digitalWrite(13, LOW);

    }
    
    if (LuxVal >= LuxHigh){
        CurrPWM = PWMbright;
    }
    else{
        CurrPWM = PWMdim;
    }

    if((Detect335) || (ForceOn)){
        LED_1.flash(CurrPWM);
        LED_2.flash(CurrPWM);
    }
    else{
        LED_1.off();
        LED_2.off();
    }

    if (millis() - LastForceOnTimer >= ForceOnTime){
        ForceOn = false;
    }

    if (millis() - SignOnTimer >= SignOnTime){
        Detect335 = false;
    }

    publishData();

    while(bootUp) {
        runOnce();
        bootUp = false;
    }
    
}