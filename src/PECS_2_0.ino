/*
 * Project PECS_2_0
 * version 1.1.1
 * Description: Base code for functionality of the Hi-Vis Group PECS 2.0 PCB
 * Author: J.Lawlor
 * Date: 01/09/2021
 */

//#pragma PARTICLE_NO_PREPROCESSOR
//The below are to always be included
#include "Particle.h"
#include "MAX14830.h"
#include "LED.h"
#include "INPUT.h"
#include "MCP3021-RK.h"
#include "MCP23017-RK.h"

//Optional includes
#include "MPU6050.h"
#include "sunset.h"
#include "SparkTime.h"

PRODUCT_ID(16216);
PRODUCT_VERSION(4);

SYSTEM_THREAD(ENABLED);

char DeviceName[32]            = "";
void handler(const char *topic, const char *data) {
  strncpy(DeviceName, data, sizeof(DeviceName)-1);
  Serial.printlnf("received %s: %s", topic, DeviceName);
}

String version = "PECS_16216_190121_JL_3";

SerialLogHandler logHandler;
//Serial1LogHandler logHandler2;

// MPU variables:
MPU6050 accelgyro;
int16_t ax, ay, az;
int16_t gx, gy, gz;

//Expansion I/O initialisation
MCP23017 gpio(Wire, 0);
MCP3021 adc(Wire);

//SPI > UART bridge initialisation
MAX14830 SPItoUART;

String ReadString335;

/* Project Variables */
int signType                    = 0;
bool signConfigured             = false;
bool type24Hour                 = false;
bool type335                    = false;
bool type318                    = false;
bool typePedest                 = false;
bool type343                    = false;
bool typeCattle                 = false;
bool typeStop                   = false;
bool typeSchool                 = false;
bool typeOutputControl          = false; 
bool typeTARP1                  = false;   

//enum deviceType {
//    typeNone, 
//    type24Hour, 
//    type335, 
//    type318, 
//    typePedest, 
//    type343, 
//    typeCattle, 
//    typeStop, 
//    typeSchool, 
//    typeOutputControl
//};


int HighSpeedThreshold          = 20;
int pinOutput1                  = D4;
int pinOutput2                  = D5;
int pinOutput3                  = D6;
int pinOutput4                  = D7;
//int pin335                    = gpio.mapPin(); // needs to be updated to the EIO pin GPA2
//int pinExLed1                 = gpio.PIN_2;
int ButtonACount                = 0;
int ButtonBCount                = 0;
int Speed 				        = 0;

/* LED Variables */
int FlashRate1                  = 500;
int FlashRate2                  = 350;
int SignOnTime                  = 3000;        // Time to hold the 335 trigger on for
unsigned long SignOnTimer       = 0;
int CurrPWM                     = 255;
int PWMdim                      = 60;
int PWMbright                   = 255;
int LuxVal;
int LuxLow                      = 249;         // Change this value to change the ambient light level crossover at which the LED's will dim
int LuxHigh                     = 250;
String OutputStates             = "0000";
int OutputState1                = atoi(OutputStates.substring(0, 1));
int OutputState2                = atoi(OutputStates.substring(1, 2));
int OutputState3                = atoi(OutputStates.substring(2, 3));
int OutputState4                = atoi(OutputStates.substring(3, 4));

/* Sign Data Variables */
int pinVoltageSense             = A0;
int pinLDR                      = A5;
float Vdivider                  = 4.43;
bool batteryLevelLow            = false;
float battReconnect             = 12.5;
float battDisconnect            = 11.6;

/* Particle Function Variables */
int ForceOn                     = 0;
int CalMode                     = 0;

/* Particle Variables */
double BattVolt             	= 0;
//double EnclosureTemp          = 0;

/* Timing Functionality Variables */
long PublishTime               = 360000; //time for particle publish functon
unsigned long LastPublish      = 0;
long SenseReadTime             = 600000; //time for sense check
unsigned long LastSenseRead    = 0;
long CalPubTime                = 1100; //time for calibration publishes
unsigned long LastCalPub       = 0;
long CalOnTmer                 = 900000; //time to stay in cal mode
unsigned long LastCalOnTmer    = 0;
long ForceOnTime               = 30000; //time to keep the force flash on
unsigned long LastForceOnTimer = 0;

/* Sensor Variables */
bool signTrigger               = false;    // Is the 335 detecting a vehicle?

/* EEPROM Variables */
int EEPROMinit;
bool EEPROMnew; // Determine if the EEPROM is new i.e. needs to be initialised or not
int EEPROMdata[] = {54, signType, signConfigured, Vdivider, FlashRate1, FlashRate2, SignOnTime, HighSpeedThreshold, OutputState1, OutputState2, OutputState3, OutputState4};

LEDClass LED_1(pinOutput1,0,FlashRate1);
LEDClass LED_2(pinOutput2,1,FlashRate2);
LEDClass LED_3(pinOutput3,0,FlashRate1);
LEDClass LED_4(pinOutput4,1,FlashRate2);

//Input ButtonLeft(7,0);
//Input ButtonRight(6,0);
int PrevButtonA = 0;
int PrevButtonB = 0;
int CurrButtonA = 0;
int CurrButtonB = 0;
bool ButtonUpdate = false;
String LEDS12 = "";
String LEDS34 = "";
String LEDS1 = "";
String LEDS24 = "";
char xbeePacket;

bool bootUp = true;

void setupUARTs() {
    SPItoUART.UART_selectUART(UART0);
    SPItoUART.UART_setBaudrate(115200);
    SPItoUART.UART_setDataLength(8);
    SPItoUART.UART_setStopBits(1);
    SPItoUART.UART_enableAutoTransceiverControl();
    SPItoUART.UART_resetFIFOs();
    
    SPItoUART.UART_selectUART(UART1);
    SPItoUART.UART_setBaudrate(115200);
    SPItoUART.UART_setDataLength(8);
    SPItoUART.UART_setStopBits(1);
    //SPItoUART.UART_enableAutoTransceiverControl();
    SPItoUART.UART_resetFIFOs();

    SPItoUART.UART_selectUART(UART2);
    SPItoUART.UART_setBaudrate(115200);
    SPItoUART.UART_setDataLength(8);
    SPItoUART.UART_setStopBits(1);
    SPItoUART.UART_enableAutoTransceiverControl();
    SPItoUART.UART_resetFIFOs();

    SPItoUART.UART_selectUART(UART3);
    SPItoUART.UART_setBaudrate(115200);
    SPItoUART.UART_setDataLength(8);
    SPItoUART.UART_setStopBits(1);
    SPItoUART.UART_enableAutoTransceiverControl();
    SPItoUART.UART_resetFIFOs();

    setupInterrupts();
}

void setupInterrupts() {
    SPItoUART.UART_IRQEnableInterrupt(LSR_INT);
    SPItoUART.UART_LSREnableInterrupt(RTIMEOUT_INT);
    SPItoUART.UART_LSREnableInterrupt(ROVERRUN_INT);
    SPItoUART.UART_LSREnableInterrupt(PARITY_INT);
    SPItoUART.UART_LSREnableInterrupt(FRAMEERR_INT);
    SPItoUART.UART_LSREnableInterrupt(RBREAK_INT);
    SPItoUART.UART_LSREnableInterrupt(RXNOISE_INT);
    
    SPItoUART.UART_registerLSRInterruptHandler(ErrorInterruptHandler);
    SPItoUART.UART_registerDummyHandler(TestInterruptHandler);

    //Log.info("UART1 Interrupt Register", String::format("%d", SPItoUART.UART_readInterrupts()));
    delay(500);
}

void runOnce() {
    setupUARTs();
    //check clock ready and wait till clock ready
    while(!SPItoUART.UART_checkClockReady()) {
        //Log.info("Waiting for clock ready", String::format("%s", SPItoUART.UART_checkClockReady() ? "true" : "false"));
        delay(1000);
    }
    //Log.info("Clock ready!", "Success");
    delay(500);

    //Log.info("MAX14830 ready for data transmission");
    delay(500);
    signSetup(EEPROM.get(4,signType));
    checkAnalogInputs();
    publishData();
}

void ErrorInterruptHandler() {
    publish("Interrupt Handler","Error Interrupt Handler");
    //Log.info("Error Interrupt Handler");
}

void TestInterruptHandler() {
    publish("Interrupt Handler", "Test Interrupt Handler");
    //Log.info("Test Interrupt Handler");
}

void startEEPROM(){
    // See if EEPROM has been initalised
    EEPROM.get(0,EEPROMinit);
    if (EEPROMinit != 54){  // Random number used to identify if EEPROM has been initalised
        int arrLen = sizeof(EEPROMdata) / sizeof(int);
        int addr = 0;
        for (int i=0; i<arrLen; i++){
            EEPROM.put(addr,EEPROMdata[i]);
            addr += sizeof(int);
        }
        EEPROMnew = 1;
    }
    //int EEPROMdata[] = {54, signType, signConfigured, Vdivider, FlashRate1, FlashRate2, SignOnTime, HighSpeedThreshold};
    else if (EEPROMinit == 54){
        EEPROM.get(4,signType);
        EEPROM.get(8,signConfigured);
        EEPROM.get(12,Vdivider);
        EEPROM.get(16,FlashRate1); // Addresses increment by 4 - sizeof(int)
        EEPROM.get(20,FlashRate2); // Addresses increment by 4 - sizeof(int)
        EEPROM.get(24,SignOnTime); // Addresses increment by 4 - sizeof(int)
        EEPROM.get(28,HighSpeedThreshold); // Addresses increment by 4 - sizeof(int)

        EEPROM.get(32,OutputState1); // Addresses increment by 4 - sizeof(int)
        EEPROM.get(36,OutputState2); // Addresses increment by 4 - sizeof(int)
        EEPROM.get(40,OutputState3); // Addresses increment by 4 - sizeof(int)
        EEPROM.get(44,OutputState4); // Addresses increment by 4 - sizeof(int)
        
        OutputStates = String(OutputState1) + String(OutputState2) + String(OutputState3) + String(OutputState4);

        EEPROMnew = 0;
        LED_1.updateFlashRate(FlashRate1);
        LED_2.updateFlashRate(FlashRate2);
    }
}

void buttonProcessing(){
    if(typeOutputControl){
    //gpio.pinMode(6, INPUT);     //RIGHT
    //gpio.pinMode(7, INPUT);     //LEFT
        LEDS12 = OutputStates.substring(0,2);
        LEDS34 = OutputStates.substring(2,4);

        CurrButtonA = gpio.digitalRead(7);
        CurrButtonB = gpio.digitalRead(6);

        if ((CurrButtonA) && (CurrButtonA != PrevButtonA)){
            if (LEDS12 == "00"){
                LEDS12 = "30";
            }
            else if(LEDS12 == "30"){
                LEDS12 = "03";
            }
            else if (LEDS12 == "03"){
                LEDS12 = "00";
            }
            Log.info("Button A " + String(ButtonACount));
            //publish(OutputStates,String(LEDS12));
            ButtonUpdate = true;
        }

        if ((CurrButtonB) && (CurrButtonB != PrevButtonB)){
            if (LEDS34 == "00"){
                LEDS34 = "30";
            }
            else if(LEDS34 == "30"){
                LEDS34 = "03";
            }
            else if (LEDS34 == "03"){
                LEDS34 = "00";
            }
            Log.info("Button B " + String(ButtonACount));
            //publish(OutputStates,String(LEDS34));
            ButtonUpdate = true;
        }
        
        if (ButtonUpdate){
            OutputStates = LEDS12 + LEDS34;
            ButtonUpdate = false;

            int OutputState1                = atoi(OutputStates.substring(0, 1));
            int OutputState2                = atoi(OutputStates.substring(1, 2));
            int OutputState3                = atoi(OutputStates.substring(2, 3));
            int OutputState4                = atoi(OutputStates.substring(3, 4));
            EEPROM.put(32,OutputState1); // Addresses increment by 4 - sizeof(int)
            EEPROM.put(36,OutputState2); // Addresses increment by 4 - sizeof(int)
            EEPROM.put(40,OutputState3); // Addresses increment by 4 - sizeof(int)
            EEPROM.put(44,OutputState4); // Addresses increment by 4 - sizeof(int)

        }
        PrevButtonA = CurrButtonA;
        PrevButtonB = CurrButtonB;
    }

    if(typeTARP1){
    //gpio.pinMode(6, INPUT);     //RIGHT
    //gpio.pinMode(7, INPUT);     //LEFT

    while (Serial1.available() > 0){
    xbeePacket = Serial1.read();
    Log.info(String(xbeePacket));
    //delay(3);
    //a = '';
    }

    LEDS1 = OutputStates.substring(0,1);
    LEDS24 = OutputStates.substring(1,4);

    if (xbeePacket == 'U' || gpio.digitalRead(7) == 1){
        CurrButtonA = 1;
    }
    else if (xbeePacket != 'U' && gpio.digitalRead(7) == 0){
        CurrButtonA = 0;

    }

    if (xbeePacket == 'L' || gpio.digitalRead(6) == 1){
        CurrButtonB = 1;
    }
    else if (xbeePacket != 'L' && gpio.digitalRead(6) == 0){
        CurrButtonB = 0;
    }
    //CurrButtonA = gpio.digitalRead(7);
    //CurrButtonB = gpio.digitalRead(6);

    if ((CurrButtonA) && (CurrButtonA != PrevButtonA)){
        if (LEDS1 == "0"){
            LEDS1 = "3";
        }
        else if(LEDS1 == "3"){
            LEDS1 = "0";
        }
        Log.info(String(xbeePacket));
        Log.info("Button A " + String(CurrButtonA));
        //publish(OutputStates,String(LEDS1));
        ButtonUpdate = true;
    }

    if ((CurrButtonB) && (CurrButtonB != PrevButtonB)){
        if (LEDS24 == "000"){
            LEDS24 = "300";
        }
        else if(LEDS24 == "300"){
            LEDS24 = "030";
        }
        else if (LEDS24 == "030"){
            LEDS24 = "003";
        }
        else if (LEDS24 == "003"){
            LEDS24 = "000";
        } 
        Log.info(String(xbeePacket));                           
        Log.info("Button B " + String(CurrButtonB));
        //publish(OutputStates,String(LEDS24));
        ButtonUpdate = true;
    }
    
    if (ButtonUpdate){
        OutputStates = LEDS1 + LEDS24;
        ButtonUpdate = false;

        int OutputState1                = atoi(OutputStates.substring(0, 1));
        int OutputState2                = atoi(OutputStates.substring(1, 2));
        int OutputState3                = atoi(OutputStates.substring(2, 3));
        int OutputState4                = atoi(OutputStates.substring(3, 4));
        EEPROM.put(32,OutputState1); // Addresses increment by 4 - sizeof(int)
        EEPROM.put(36,OutputState2); // Addresses increment by 4 - sizeof(int)
        EEPROM.put(40,OutputState3); // Addresses increment by 4 - sizeof(int)
        EEPROM.put(44,OutputState4); // Addresses increment by 4 - sizeof(int)

    }
        PrevButtonA = CurrButtonA;
        PrevButtonB = CurrButtonB;
        xbeePacket = '0';
    }
}

void outputCommandProcessing(String OutputCommand){
    if(typeOutputControl){

        int CommandLength = OutputCommand.length();
        //for (int i=0; i<CommandLength; i++){
        //publish(String(i), OutputCommand.substring(i, i+1));
        
        //left hand side of sign
        if (OutputCommand.substring(0,1) == "3"){
            LED_1.flash(CurrPWM);
            LED_2.off();
            //publish("1 ON", " 2 OFF");
        }

        else if (OutputCommand.substring(0,1) == "0"){
            LED_1.off();
        }
        
        if (OutputCommand.substring(1,2) == "3"){
            LED_2.flash(CurrPWM);
            LED_1.off();
            //publish("2 ON", " 1 OFF");
        }

        else if (OutputCommand.substring(1,2) == "0"){
            LED_2.off();
        }

        //right hand side of sign
        if (OutputCommand.substring(2,3) == "3"){
            LED_3.flash(CurrPWM);
            LED_4.off();
            //publish("3 ON", " 4 OFF");
        }

        else if (OutputCommand.substring(2,3) == "0"){
            LED_3.off();
        }

        if (OutputCommand.substring(3,4) == "3"){
            LED_4.flash(CurrPWM);
            LED_3.off();
            //publish("4 ON", " 3 OFF");
        }

        else if (OutputCommand.substring(3,4) == "0"){
            LED_4.off();
        }
    }
}

void TARP1Processing(String OutputCommand){
    if(typeTARP1){
        //Blue, White, Orange, Red
        int CommandLength = OutputCommand.length();
        //for (int i=0; i<CommandLength; i++){
        //publish(String(i), OutputCommand.substring(i, i+1));
        
        //left hand side of sign
        if (OutputCommand.substring(0,1) == "3"){
            LED_1.flash(CurrPWM);
            //publish("LED", "1 ON");
        }

        else if (OutputCommand.substring(0,1) == "0"){
            LED_1.off();
        }
        
        if (OutputCommand.substring(1,2) == "3"){
            LED_2.flash(CurrPWM);
            LED_3.off();
            LED_4.off();
            //publish("LED", "2 ON");
        }

        else if (OutputCommand.substring(1,2) == "0"){
            LED_2.off();
        }

        //right hand side of sign
        if (OutputCommand.substring(2,3) == "3"){
            LED_3.flash(CurrPWM);
            LED_4.off();
            LED_2.off();
            //publish("LED", "3 ON");
        }

        else if (OutputCommand.substring(2,3) == "0"){
            LED_3.off();
        }

        if (OutputCommand.substring(3,4) == "3"){
            LED_4.flash(CurrPWM);
            LED_3.off();
            LED_2.off();
            //publish("LED", " 3 OFF");
        }

        else if (OutputCommand.substring(3,4) == "0"){
            LED_4.off();
        }
    }
}

int outputControl(String OutputCommand){
   if (typeOutputControl || typeTARP1){
        if(OutputCommand != OutputStates && OutputCommand.length() == 4){
            publish("New Output Command", OutputCommand);
            //OutputStates = "";
            //outputCommandProcessing(OutputCommand);
            OutputStates = OutputCommand;
            publish("Output States", OutputStates);
            //EEPROM.put(32,OutputStates);
            int OutputState1                = atoi(OutputStates.substring(0, 1));
            int OutputState2                = atoi(OutputStates.substring(1, 2));
            int OutputState3                = atoi(OutputStates.substring(2, 3));
            int OutputState4                = atoi(OutputStates.substring(3, 4));
            EEPROM.put(32,OutputState1); // Addresses increment by 4 - sizeof(int)
            EEPROM.put(36,OutputState2); // Addresses increment by 4 - sizeof(int)
            EEPROM.put(40,OutputState3); // Addresses increment by 4 - sizeof(int)
            EEPROM.put(44,OutputState4); // Addresses increment by 4 - sizeof(int)
            //OutputStates = OutputCommand;
            return 0;
        }

        else if (OutputCommand == OutputStates){
            //publish("Output Control", "No state change made");
        
            return 1;
        }
   }
   else{
       return -1;
       //publish("Output Control", "Not Enabled");
   }
}

int resetEEPROM(String Reset){
    bool ResetEEPROM = atoi(Reset);
    if (ResetEEPROM == 1){
        EEPROM.clear();
        if(Particle.connected()){
    	    publish("EEPROM", "Clearing EEPROM");
        }
        System.reset(); //reboot to after clearing EEPROM to allow changes to take affect
    }
    return ResetEEPROM;
}

void calMode(String TriggerType, String Data) {
	if (millis() - LastCalOnTmer < CalOnTmer){
	    if (Particle.connected() && CalMode == 1 && ((millis() - LastCalPub >= CalPubTime))){
	        publish(TriggerType, Data);
			CalMode = 1;
	        LastCalPub = millis();
	    }
	}
	else CalMode = 0;
}

int calibration(String Mode){													// setting device to calibration mode will report device functional status to Console for debugging
    CalMode = atoi(Mode);
    if (CalMode == 1){
        LastCalOnTmer = millis();
        if(Particle.connected()){
    	       publish("Calibration", "Entering Cal Mode");
        }
    }
	else if (CalMode == 0 && Particle.connected()){
        publish("Calibration", "Exiting Cal Mode");
    }
    return CalMode;
}

int forceOn(String force){													    // Forces local activation of the sign to prove local functionality
    ForceOn = atoi(force);
    if (ForceOn == 1){
        LastForceOnTimer = millis();
    }
    if(Particle.connected()){
        Particle.publish("Force Flash", String(ForceOn));
    }
    return ForceOn;
}

int power335(String pwr){                                                       // Power the radar through on board MOSFET 
    int State335 = atoi(pwr);
    gpio.digitalWrite(2, State335);
    return State335;
}

int setHST(String HST){													        // Forces local activation of the sign to prove local functionality
    HighSpeedThreshold = atoi(HST);
    EEPROM.put(28,HighSpeedThreshold);
    return HighSpeedThreshold;
}

int setSignOnTime(String on){													// Sets the amount of time each trigger event will activate the sign for
    SignOnTime = atoi(on);
    EEPROM.put(24,SignOnTime);
    return SignOnTime;
}

int setFlashRate1(String flash){												// Sets the flash rate of light 1 via cloud function 
    FlashRate1 = atoi(flash);
    LED_1.updateFlashRate(atoi(flash));
    EEPROM.put(16,FlashRate1);
    return FlashRate1;
}

int setFlashRate2(String flash){												// Sets the flash rate of light 2 via cloud function
    FlashRate2 = atoi(flash);
    LED_2.updateFlashRate(atoi(flash));
    EEPROM.put(20,FlashRate2);
    return FlashRate2;
}

float voltCal(String volt){
    Vdivider = volt.toFloat();
    EEPROM.put(12,Vdivider);
    return Vdivider;

}

int setSignType(String sign){											// Sets the flash rate of light 2 via cloud function
    if (atoi(sign) != signType){
        bool typeChange = true;
        signType = atoi(sign);
        EEPROM.put(4,signType);
        EEPROM.put(8, false);
        publish("Sign Config", "Type Changed to " + String(signType));
        publish("Sign Config", "Setting up new sign type");
        publish("Sign Config", "Please enter Sign Config data");
        //Log.info(EEPROMdata);
        
        signSetup(signType);

    }
    else{
        //sign type has not changed
        publish("Sign Config", "No changes. Select new type to change");
        publish("Sign Config", "No change to sign configuration");

    }

    return signType;
}

void signSetup(int signType){
    switch (signType)
    {
    case 0: 
        publish("Configuring", "24 Hour Flashing");
        type24Hour = true; //must be an easier way to set these to true / false iterating through any that are not the case being set? someone smarter than me can sort it hopefully...
        type335 = false;
        type318 = false;
        typePedest = false;
        typeCattle = false; 
        typeStop = false;
        type343 = false;
        typeSchool = false;
        typeOutputControl = false;
        //signHealthCheck(signType)
        //setParameters(/*data to set to new sign type goes here*/);
        break;
    case 1: 
        publish("Configuring", "335 Radar System");
        //radarSetup(signType);
        gpio.digitalWrite(2, HIGH);
        delay(3000);
        SPItoUART.UART_selectUART(UART1);
        SPItoUART.UART_setBaudrate(115200);
        SPItoUART.UART_setDataLength(8);
        SPItoUART.UART_setStopBits(1);
        SPItoUART.UART_resetFIFOs();
        publish("Radar Setup", "Configuring for AGD 335");
        publish("Comms Setup", "MAX UART1, 115200,8,1,NONE");
        publish("Radar Test", "Testing for AGD335 at UART1");
        burstWriteUART(1, "AGD\r");
        check335();
        type24Hour = false; //must be an easier way to set these to true / false iterating through any that are not the case being set? someone smarter than me can sort it hopefully...
        type335 = true;
        type318 = false;
        typePedest = false;
        typeCattle = false; 
        typeStop = false;
        type343 = false;
        typeSchool = false;
        typeOutputControl = false;
        typeTARP1 = false;
        //setParameters(/*data to set to new sign type goes here*/);
        break;
    case 2: 
        publish("Configuring", "318 Radar System");
        type24Hour = false; //must be an easier way to set these to true / false iterating through any that are not the case being set? someone smarter than me can sort it hopefully...
        type335 = false;
        type318 = true;
        typePedest = false;
        typeCattle = false; 
        typeStop = false;
        type343 = false;
        typeSchool = false;
        typeOutputControl = false;
        typeTARP1 = false;
        //signHealthCheck(signType)
        //setParameters(/*data to set to new sign type goes here*/);
        break;
    case 3: 
        publish("Configuring", "343 Radar System");
        type24Hour = false; //must be an easier way to set these to true / false iterating through any that are not the case being set? someone smarter than me can sort it hopefully...
        type335 = false;
        type318 = false;
        typePedest = false;
        typeCattle = false; 
        typeStop = false;
        type343 = true;
        typeSchool = false;
        typeOutputControl = false;
        typeTARP1 = false;
        //signHealthCheck(signType)
        //setParameters(/*data to set to new sign type goes here*/);
        break;
    case 4: 
        publish("Configuring", "Cattle Crossing System");
        type24Hour = false; //must be an easier way to set these to true / false iterating through any that are not the case being set? someone smarter than me can sort it hopefully...
        type335 = false;
        type318 = false;
        typePedest = false;
        typeCattle = true; 
        typeStop = false;
        type343 = false;
        typeSchool = false;
        typeOutputControl = false;
        typeTARP1 = false;
        //signHealthCheck(signType)
        //setParameters(/*data to set to new sign type goes here*/);
        break;
    case 5:
        publish("Configuring", "Pedestrian System");
        type24Hour = false; //must be an easier way to set these to true / false iterating through any that are not the case being set? someone smarter than me can sort it hopefully...
        type335 = false;
        type318 = false;
        typePedest = true;
        typeCattle = false; 
        typeStop = false;
        type343 = false;
        typeSchool = false;
        typeOutputControl = false;
        typeTARP1 = false;
        //signHealthCheck(signType)
        //setParameters(/*data to set to new sign type goes here*/);
        break;
    case 6:
        publish("Configuring", "Stop Line Detection System");
        type24Hour = false; //must be an easier way to set these to true / false iterating through any that are not the case being set? someone smarter than me can sort it hopefully...
        type335 = false;
        type318 = false;
        typePedest = false;
        typeCattle = false; 
        typeStop = true;
        type343 = false;
        typeSchool = false;
        typeOutputControl = false;
        typeTARP1 = false;
        //signHealthCheck(signType)
        //setParameters(/*data to set to new sign type goes here*/);
        break;
    
    case 7:
        publish("Configuring", "School Zone");
        type24Hour = false; //must be an easier way to set these to true / false iterating through any that are not the case being set? someone smarter than me can sort it hopefully...
        type335 = false;
        type318 = false;
        typePedest = false;
        typeCattle = false; 
        typeStop = false;
        type343 = false;
        typeSchool = true;
        typeOutputControl = false;
        typeTARP1 = false;

        //signHealthCheck(signType)
        //setParameters(/*data to set to new sign type goes here*/);
        break;
    
    case 8:
        publish("Configuring", "Output Control");
        type24Hour = false; //must be an easier way to set these to true / false iterating through any that are not the case being set? someone smarter than me can sort it hopefully...
        type335 = false;
        type318 = false;
        typePedest = false;
        typeCattle = false; 
        typeStop = false;
        type343 = false;
        typeSchool = false;
        typeOutputControl = true;
        typeTARP1 = false;
        //signHealthCheck(signType)
        //setParameters(/*data to set to new sign type goes here*/);
        break;

    case 9:
        publish("Configuring", "TARP type 1");
        type24Hour = false; //must be an easier way to set these to true / false iterating through any that are not the case being set? someone smarter than me can sort it hopefully...
        type335 = false;
        type318 = false;
        typePedest = false;
        typeCattle = false; 
        typeStop = false;
        type343 = false;
        typeSchool = false;
        typeOutputControl = false;
        typeTARP1 = true;
        //signHealthCheck(signType)
        //setParameters(/*data to set to new sign type goes here*/);
        break;
    
    default:
        publish("Invalid Selection", "Please select valid type");
        //signHealthCheck(signType)
        //setParameters(/*data to set to new sign type goes here*/);
        break;
    }
}

void signHealthCheck(){ // this will check relevan healt aspects for signtype

}

void radarSetup(int signType){ // this function may be  made redundant by doing the checks in the swtich - signSetup()
    if (signType = 1){
        SPItoUART.UART_selectUART(UART1);
        SPItoUART.UART_setBaudrate(115200);
        SPItoUART.UART_setDataLength(8);
        SPItoUART.UART_setStopBits(1);
        SPItoUART.UART_resetFIFOs();
        publish("Radar Setup", "Configuring for AGD 335");
        publish("Comms Setup", "MAX UART1, 115200,8,1,NONE");
        publish("Radar Test", "Testing for AGD335 at UART1");
        burstWriteUART(1, "AGD\r");
        check335();
    }
}

void publish(String name, String data){
    bool publishReady;
    if (Particle.connected()){
        particle::Future<bool> publishInProgress = Particle.publish(name, data, PRIVATE, WITH_ACK);
        while (!publishInProgress.isDone())
        {
            publishReady = false;
        }
        if(publishInProgress.isSucceeded()){
            publishReady = true;
        }
    }

}

void check335(){
    gpio.digitalWrite(2, HIGH);
    while (SPItoUART.UART_getRxFIFOLevel(UART1) > 0){
        char c = SPItoUART.UART_readByte(UART1);
        ReadString335 += c;
        if (c == '\r' && ReadString335.length() == 6){
            //Log.info(ReadString335);
            Speed = ReadString335.substring(2).toInt();
            if (Speed >= HighSpeedThreshold){
                //Log.info(String(Speed));
                signTrigger = true;
                SignOnTimer = millis();
                calMode("Detect335", String(Speed));
            }
            ReadString335 = "";
        }

        else if (c == '\r' && ReadString335.length() > 6){
            //Log.info(ReadString335);
            publish("335 Data", ReadString335);
            ReadString335 = "";
        }

        else if (c == '\r' && ReadString335.length() < 6){
                //Log.info(ReadString335);
                ReadString335 = "";
            }

        else if (c == 'C'){
            if (Particle.connected()){
                publish("Activated",String(Speed));
                ReadString335 = "";
            }
        }
    }
}


void publishData(){
    String JsonHealthString =
    String("{ \"Device\":\"" + String(DeviceName) + "\""
        + ",\"Battery_Voltage\":" + String(BattVolt,2)
        //+ ",\"Enclosure_Temp\":" + String(EnclosureTemp,2)
        + ",\"Light_Sensor\":" + String(float(LuxVal) /4095, 2)
        + ",\"version\":\"" + String(version) + "\""
        + "}");

    if (((millis() - LastPublish >= PublishTime) && (Particle.connected())) || bootUp) {
        Particle.publishVitals();
        Particle.publish("particle/device/name");
        Particle.publish("Health", JsonHealthString, PRIVATE);
        LastPublish = millis();
    }
}

void checkAnalogInputs(){
    LuxVal = analogRead(pinLDR);
    BattVolt = ((analogRead(pinVoltageSense)*3.3)/4095*Vdivider);
}

void burstWriteUART(int uart, String in){
    byte txStr[in.length()];
    in.getBytes(txStr, in.length() + 1);
    //Log.info(in);
    if(uart = 0){
        SPItoUART.UART_writeBurst(UART0, (uint8_t *) txStr, in.length() + 1);
    }
    else if (uart = 1){
        SPItoUART.UART_writeBurst(UART1, (uint8_t *) txStr, in.length() + 1);
    }
    else if (uart = 2){
        SPItoUART.UART_writeBurst(UART2, (uint8_t *) txStr, in.length() + 1);
    }
    else if (uart = 3){
        SPItoUART.UART_writeBurst(UART3, (uint8_t *) txStr, in.length() + 1);
    }
}

void setup() {
    Particle.subscribe("particle/device/name", handler);			// Master device that this slave will subscribe to and the string that needs to be publish
    Particle.publish("particle/device/name");

    Particle.function("Set_Sign_Type", setSignType);
    Particle.function("Calibration",calibration);
    Particle.function("High_Speed_Threshold", setHST);
    Particle.function("Flash_Rate_1",setFlashRate1);
    Particle.function("Flash_Rate_2",setFlashRate2);
    Particle.function("On_Time",setSignOnTime);
    Particle.function("Force_On",forceOn);
    //Particle.function("335_Power",power335);
    Particle.function("Output_Control",outputControl);
    Particle.function("Reset_EEPROM", resetEEPROM);
    Particle.function("Voltage_Cailbrate", voltCal);

    Particle.variable("Light_Sense",LuxVal);
    Particle.variable("Batt_Volt",BattVolt);
    //Particle.variable("EEPROMdata", (EEPROMdata));
    Particle.variable("High_Speed_Threshold",HighSpeedThreshold);
    Particle.variable("Output_States",OutputStates);
    Particle.variable("SoftVer", version);
    Serial1.begin(9600);
    Serial1.setTimeout(500);
    Wire.begin();
    
    pinMode(pinOutput1, OUTPUT); 
    pinMode(pinOutput2, OUTPUT);
    pinMode(pinOutput3, OUTPUT); 
    pinMode(pinOutput4, OUTPUT);
    gpio.begin(); // start I2C wire comms with Expansion GPIO then initialise the GPIO
    gpio.pinMode(6, INPUT);     //SWITCH S5 (RIGHT)
    gpio.pinMode(7, INPUT);     //SWITCH S6 (LEFT)
    gpio.pinMode(12, OUTPUT);   //LED 1
    gpio.pinMode(13, OUTPUT);   //LED 2
    gpio.pinMode(14, OUTPUT);   //LED 3
    gpio.pinMode(1, OUTPUT);    //OUTPUT 5 
    gpio.pinMode(2, OUTPUT);    //OUTPUT 6

    //gpio.digitalWrite(2, HIGH); //turn on power to radar at OUT6 on PCB

    startEEPROM();
}

void batteryLow(){
    digitalWrite(pinOutput1, LOW);
    digitalWrite(pinOutput2, LOW);
    digitalWrite(pinOutput3, LOW);
    digitalWrite(pinOutput4, LOW);
    //gpio.digitalWrite(1, LOW);
    //gpio.digitalWrite(2, LOW);
}

void loop() {

//battery discon - recon checks

    if(((analogRead(pinVoltageSense)*3.3)/4095*Vdivider) < battDisconnect){
        batteryLevelLow = true;
        batteryLow();
        //publish("Battery Disconnect",String(((analogRead(pinVoltageSense)*3.3)/4095*Vdivider)));
        gpio.digitalWrite(12, HIGH);
    }

    else if (((analogRead(pinVoltageSense)*3.3)/4095*Vdivider) > battReconnect){
        batteryLevelLow = false;
        gpio.digitalWrite(12, LOW);
        //publish("Battery Reconnect",String(((analogRead(pinVoltageSense)*3.3)/4095*Vdivider)));
        //signSetup(EEPROM.get(4,signType));
        
    }

    else{
        //publish("Battery Limbo",String(((analogRead(pinVoltageSense)*3.3)/4095*Vdivider)));
    }

    if (analogRead(pinLDR) >= LuxHigh){
        CurrPWM = PWMbright;
    }
    else{
        CurrPWM = PWMdim;
    }
/*
    if(!EEPROM.get(8,signConfigured)){
        publish("Sign Config", "Sign Not Configured, Select Type");
    }
*/
// operation dependant on sign type
    
    if (!batteryLevelLow){

        buttonProcessing();
        outputCommandProcessing(OutputStates);
        TARP1Processing(OutputStates);

        if(EEPROM.get(4,signType) == 1){
            check335();
        }
        else if(typeOutputControl || typeTARP1){
            //outputProcessing();
        }
        else if(type24Hour){
            signTrigger = true;
        }
        

        if(signTrigger || ForceOn){
            LED_1.flash(CurrPWM);
            LED_2.flash(CurrPWM);
        }
        else{
            if (signType != 8 && signType != 9){ // stops conflict with LEDs for different sign types
                LED_1.off();
                LED_2.off();
            }
        }

        if (millis() - LastForceOnTimer >= ForceOnTime){
            ForceOn = false;
        }

        if (millis() - SignOnTimer >= SignOnTime){
            signTrigger = false;
        }
    }

    checkAnalogInputs();
    publishData();

    while(bootUp) {
        runOnce();
        bootUp = false;
    }
    
}